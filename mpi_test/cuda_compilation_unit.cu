#include "cuda_compilation_unit.hpp"

#include <cuda_runtime.h>

void cuda_compilation_unit_function() {
  // Some runtime calls, for example
  int *p;
  cudaMalloc(&p, sizeof(int));
  cudaFree(p);
}
