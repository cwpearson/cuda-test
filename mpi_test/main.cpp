#include <mpi.h>
#include "cuda_compilation_unit.hpp"

int main(int argc, char **argv) {

  MPI_Init(&argc, &argv);

  // Get my rank
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  std::cout << "Rank " << rank << " reporting!" << std::endl;
  cuda_compilation_unit_function();

  MPI_Finalize();
}
