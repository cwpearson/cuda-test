#include <cstdio>

#define CUDA_CHECK(ans)                                                        \
  { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line,
                      bool abort = true) {
  if (code != cudaSuccess) {
    fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file,
            line);
    if (abort)
      exit(code);
  }
}

__global__ void childKernel(int *a, const size_t depth) {

  (*a)++;

  if (depth > 0) {
    childKernel<<<1, 2>>>(a, depth - 1);
  }
}

__global__ void kernel(int *a, const size_t depth) {
  *a = 0;
  if (depth > 0) {
    childKernel<<<1, 2>>>(a, depth - 1);
  }
}

int main(int argc, char **argv) {
  int *a_d, a_h;
  CUDA_CHECK(cudaMalloc(&a_d, sizeof(int)));

  kernel<<<1, 1>>>(a_d, 5); // test kernel launch
  CUDA_CHECK(cudaPeekAtLastError());
  CUDA_CHECK(cudaDeviceSynchronize());

  CUDA_CHECK(cudaMemcpy(&a_h, a_d, sizeof(int), cudaMemcpyDeviceToHost));
  printf("%d\n", a_h);
  CUDA_CHECK(cudaFree(a_d));
  return 0;
}
