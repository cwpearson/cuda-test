CMD="
./smem-write
"

nvprof -f --output-profile timeline.nvprof $CMD
nvprof -f --analysis-metrics --output-profile analysis.nvprof $CMD
