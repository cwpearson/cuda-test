#include <cstdio>

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

template <int BLOCK_SIZE>
__global__ void simple(int *out, const long int val, int n) {
  __shared__ int s[BLOCK_SIZE];
  const int tx = threadIdx.x;
  s[tx] = tx;
  if (threadIdx.x > 0 && threadIdx.x < BLOCK_SIZE - 1) {
    for (int iter = 0; iter < n; ++iter) {
      s[tx] = (s[tx+1] + s[tx-1]) / 2; 
    }
  }
  int x = blockIdx.x * BLOCK_SIZE + threadIdx.x;
  if ( x < n) {
    out[x] = s[threadIdx.x];
  }
}


int main(int argc, char **argv) {
  gpuErrchk( cudaDeviceReset() );

  int *dst;
  const int n = 1024 * 128;
  gpuErrchk( cudaMalloc(&dst, sizeof(int) * n) );

  // Simple
  {
    const int block_size = 256;
    dim3 blockDim(block_size);
    dim3 gridDim((n + block_size - 1) / block_size);
    simple<block_size><<<gridDim, blockDim>>>(dst, 13, 10000);
    gpuErrchk ( cudaGetLastError() );
    gpuErrchk ( cudaDeviceSynchronize() );
  }

  gpuErrchk( cudaDeviceReset() );
  return 0;
}

