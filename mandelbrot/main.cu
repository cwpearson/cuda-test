#include <iostream>
#include <cstdio>
#include <cuComplex.h>
#include <cassert>

#include "tictoc.hpp"
#include "cuda_check.hpp"

const size_t OUTPUT_SIZE_X = 4096;
const size_t OUTPUT_SIZE_Y = 4096;

const size_t ASCII_VIEW_Y = 30;
const size_t ASCII_VIEW_X = 55;

__device__ float cuCNormf(const cuComplex c) {
  const float r = cuCrealf(c);
  const float i = cuCimagf(c);
  return sqrt(r*r + i*i);
}

__device__ cuComplex getComplexFromIndex(const size_t x, const size_t y) {
  const cuComplex TL = make_cuComplex(-1.50, -1.5); // Top left
  const cuComplex BR = make_cuComplex(0.75, 1.5);   // Bottom right

  const float real = static_cast<float>(x) / OUTPUT_SIZE_X * (cuCrealf(BR)-cuCrealf(TL)) + cuCrealf(TL);
  const float imag = static_cast<float>(y) / OUTPUT_SIZE_Y * (cuCimagf(BR)-cuCimagf(TL)) + cuCimagf(TL);
  return make_cuComplex(real,imag);
}

// Calculate escape iters, capping at maxIters
__device__ unsigned char iters(const cuComplex c, const unsigned char maxIters) {
  cuComplex z = make_cuComplex(0, 0);
  for (unsigned char i = 0; i < maxIters; ++i) {
    if (cuCNormf(z) > 4.0) return i;
    z = cuCaddf(cuCmulf(z,z), c);
  }
 
  return maxIters;
}

template <int BLOCK_X, int BLOCK_Y>
__global__ void mandelbrot(unsigned char* output, int output_max_x, int output_max_y) {
  
  //starting index of each block
  int idx = blockIdx.x * BLOCK_X + threadIdx.x;
  int idy = blockIdx.y * BLOCK_Y + threadIdx.y;

  if(idx < output_max_x  && idy < output_max_y)
  {
	const cuComplex c = getComplexFromIndex(idx,idy);
        output[idy * OUTPUT_SIZE_X + idx] =  iters(c, 255);	
  }
}



int main(int argc, char**argv) {

    char* B_h = new char[OUTPUT_SIZE_Y * OUTPUT_SIZE_X];

    printf("Size y=%lu, x=%lu\n", OUTPUT_SIZE_X, OUTPUT_SIZE_Y);

    // Allocate device variables ----------------------------------------------


    unsigned char* B_d;
    CUDA_CHECK(cudaMalloc(&B_d, sizeof(unsigned char)*OUTPUT_SIZE_X*OUTPUT_SIZE_Y));


    const int block_x = 32;
    const int block_y = 8;
    dim3 gridDim((OUTPUT_SIZE_X + block_x - 1)/block_x, (OUTPUT_SIZE_Y + block_y - 1)/block_y, 1);
    dim3 blockDim(block_x, block_y, 1);
    double start = tic();
    mandelbrot<block_x, block_y><<< gridDim, blockDim >>> (B_d, OUTPUT_SIZE_X, OUTPUT_SIZE_Y);
    CUDA_CHECK(cudaGetLastError());
    CUDA_CHECK(cudaDeviceSynchronize());
    double stop = tic();
    printf("Kernel : %f MP/s\n", OUTPUT_SIZE_X * OUTPUT_SIZE_Y / (stop - start) / 1e6);

    CUDA_CHECK(cudaMemcpy(B_h, B_d, sizeof(char)*OUTPUT_SIZE_X*OUTPUT_SIZE_Y, cudaMemcpyDeviceToHost));

    for (size_t y = 0; y < OUTPUT_SIZE_Y; y += OUTPUT_SIZE_Y/ASCII_VIEW_Y) {
      for (size_t x = 0; x < OUTPUT_SIZE_X; x += OUTPUT_SIZE_X/ASCII_VIEW_X) {
        if (B_h[y * OUTPUT_SIZE_X + x] > 1) std::cout << " ";
      else                                           std::cout << "*";
      }
      std::cout << "\n";
    }

    free(B_h);
    cudaFree(B_d);

    return 0;
}
