#include "tictoc.hpp"
#include <cstdlib>
#include "sys/time.h"

using namespace std;

double tic(void) {
  timeval stop;
  gettimeofday(&stop, NULL);
  return stop.tv_sec + stop.tv_usec / 1e6;

}
