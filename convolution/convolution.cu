#include <cstdio>

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

const int KERNEL_SIZE = 7;

__constant__ float M_c[KERNEL_SIZE][KERNEL_SIZE];

__global__ void convolution_tiled(float *P, const float * N, const int size_x, const int size_y, const int tile_size, const int block_size) {
  extern __shared__ float N_s[];
  
  const int outRow = blockIdx.y*tile_size+threadIdx.y;
  const int outCol = blockIdx.x*tile_size+threadIdx.x;
  
  const int inRow = outRow-KERNEL_SIZE/2;
  const int inCol = outCol-KERNEL_SIZE/2;
  
  float output = 0.0f;
  
  if(inRow >= 0 && inRow < size_y && inCol >= 0 && inCol < size_x)
  	N_s[threadIdx.y * block_size + threadIdx.x] = N[inRow*size_x+inCol];
  else
  	N_s[threadIdx.y * block_size + threadIdx.x] = 0.0f;
  
  __syncthreads();
  
  if(threadIdx.x < tile_size && threadIdx.y < tile_size) {
  	for(int i=0; i<KERNEL_SIZE; i++)
  		for(int j=0; j<KERNEL_SIZE; j++)
  			output += M_c[i][j]*N_s[(i+threadIdx.y) * block_size + j+threadIdx.x];
  
  	if(outRow < size_y && outCol < size_x)
  		P[outRow*size_x+outCol] = output;
  }

}


__global__ void convolution_simple(float *P, const float * N, const int size_x, const int size_y, const int tile_size) {
  
  const int outRow = blockIdx.y*tile_size+threadIdx.y;
  const int outCol = blockIdx.x*tile_size+threadIdx.x;
  
  const int inRow = outRow-KERNEL_SIZE/2;
  const int inCol = outCol-KERNEL_SIZE/2;
  
  float output = 0.0f;
  
  if(threadIdx.x < tile_size && threadIdx.y < tile_size) {
    if (inRow >=0 && inRow < size_y && inCol >= 0 && inCol < size_x) {
      for(int i=0; i<KERNEL_SIZE; i++) {
        for(int j=0; j<KERNEL_SIZE; j++) {
          output += M_c[i][j]*N[(i + inRow) * size_x + (j + inCol)];
        }
      }
    }
    if(outRow < size_y && outCol < size_x) {
      P[outRow*size_x+outCol] = output;
    }
  }

}


void run_with_tile_size(const int tile_size) {
  const int size_x = 1024;
  const int size_y = 1024;

  float *input_h  = (float*) malloc(size_x * size_y * sizeof(float));
  float *output_h = (float*) malloc(size_x * size_y * sizeof(float));
  float *input_d, *output_d;
  gpuErrchk( cudaMalloc(&input_d , size_x * size_y * sizeof(float)) );
  gpuErrchk( cudaMalloc(&output_d, size_x * size_y * sizeof(float)) );  

  float kernel_h[KERNEL_SIZE][KERNEL_SIZE];

  gpuErrchk( cudaMemcpyToSymbol(M_c, kernel_h, KERNEL_SIZE*KERNEL_SIZE * sizeof(float)));

  const int block_size = tile_size + KERNEL_SIZE - 1;

  dim3 blockDim(block_size, block_size);
  dim3 gridDim(
    (size_x + tile_size - 1) / tile_size,
    (size_y + tile_size - 1) / tile_size
  );

  const int shared_size = sizeof(float) * block_size * block_size;
  convolution_tiled<<<gridDim, blockDim, shared_size, 0>>>(
    output_d, input_d, size_x, size_y, tile_size, block_size
  );
  gpuErrchk(cudaGetLastError());
  gpuErrchk(cudaDeviceSynchronize()); 
  convolution_simple<<<gridDim, blockDim>>>(output_d, input_d, size_x, size_y, tile_size);
  gpuErrchk(cudaGetLastError());
  gpuErrchk(cudaDeviceSynchronize()); 

  free(input_h);
  free(output_h); 
  gpuErrchk(cudaFree(input_d)); 
  gpuErrchk(cudaFree(output_d)); 
}


int main(int argc, char **argv) {
  for (int i = 1; i  < argc; ++i) {
    run_with_tile_size(std::atoi(argv[i]));
  }
  return 0;
}

