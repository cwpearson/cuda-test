CMD="
./convolution 6 10 14 18 22 26 28
"

  aprun -b nvprof --output-profile timeline.conv.nvprof $CMD
  aprun -b nvprof --analysis-metrics --output-profile analysis.conv.nvprof $CMD
