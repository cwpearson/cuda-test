#include <cstdio>

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

__global__ void simple(long int *dst, const long int val, int n) {
  int i = blockDim.x * blockIdx.x + threadIdx.x;
  if (i < n) {
    dst[i] = val;
  }
}

template<int SIZE_X, int SIZE_Y>
__global__ void coalesced(long int *dst, const long int val) {
  int x = blockDim.x * blockIdx.x + threadIdx.x;
  int y = blockDim.y * blockIdx.y + threadIdx.y;
  if (x < SIZE_X && y < SIZE_Y) {
    dst[y * SIZE_X + x] = val;
  }
}

template<int SIZE_X, int SIZE_Y, int BLOCK_X, int BLOCK_Y>
__global__ void coalesced_shared(long int *dst, const long int val) {
  __shared__ int s[BLOCK_Y][BLOCK_X];

  const int x = blockDim.x * blockIdx.x + threadIdx.x;
  const int y = blockDim.y * blockIdx.y + threadIdx.y;

  if (threadIdx.x < BLOCK_X && threadIdx.y < BLOCK_Y) {
    s[threadIdx.y][threadIdx.x] = val;
  }
  __syncthreads();

  if (x < SIZE_X && y < SIZE_Y) {
    dst[y * SIZE_X + x] = s[threadIdx.y][threadIdx.x];
  }
}

template<int SIZE_X, int SIZE_Y>
__global__ void coalesced_coarse(long int *dst, const long int val) {
  int y = blockDim.y * blockIdx.y + threadIdx.y;
  if (y < SIZE_Y) {
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    if (x < SIZE_X) {
      dst[y * SIZE_X + x] = val;
    }
    x += blockDim.x * gridDim.x;
    if (x < SIZE_X) {
      dst[y * SIZE_X + x] = val;
    }
  }
}

template<int SIZE_X, int SIZE_Y>
__global__ void uncoalesced(long int *dst, const long int val) {
  int y = blockDim.x * blockIdx.x + threadIdx.x;
  int x = blockDim.y * blockIdx.y + threadIdx.y;
  if (x < SIZE_X && y < SIZE_Y) {
    dst[y * SIZE_X + x] = val;
  }
}

int main(int argc, char **argv) {

  long int *dst;
  const int size_x = 4096;
  const int size_y = 4096;
  gpuErrchk ( cudaMalloc(&dst, sizeof(long int) * size_x * size_y));

  // Simple
  {
    dim3 blockDim(512);
    dim3 gridDim((size_x * size_y + blockDim.x - 1) / blockDim.x);
    simple<<<gridDim, blockDim>>>(dst, 13, size_x * size_y);
    gpuErrchk ( cudaGetLastError() );
    gpuErrchk ( cudaDeviceSynchronize() );
  }
  // Uncoalesced
  {
    dim3 blockDim(32, 16);
    dim3 gridDim((size_y + blockDim.x - 1) / blockDim.x
                ,(size_x + blockDim.y - 1) / blockDim.y);
    uncoalesced<size_x, size_y><<<gridDim, blockDim>>>(dst, 15);
    gpuErrchk ( cudaGetLastError() );
    gpuErrchk ( cudaDeviceSynchronize() );
  }
  {
    dim3 blockDim(32, 16);
    dim3 gridDim((size_x + blockDim.x - 1) / blockDim.x
                ,(size_y + blockDim.y - 1) / blockDim.y);
    coalesced<size_x, size_y><<<gridDim, blockDim>>>(dst, 17);
    gpuErrchk ( cudaGetLastError() );
    gpuErrchk ( cudaDeviceSynchronize() );
  }
  {
    const int block_x = 32;
    const int block_y = 16;
    dim3 blockDim(block_x, block_y);
    dim3 gridDim((size_x + block_x - 1) / block_x
                ,(size_y + block_y - 1) / block_y);
    coalesced_shared<size_x, size_y, block_x, block_y><<<gridDim, blockDim>>>(dst, 17);
    gpuErrchk ( cudaGetLastError() );
    gpuErrchk ( cudaDeviceSynchronize() );
  }
  {
    dim3 blockDim(32, 16);
    dim3 gridDim((size_x/2 + blockDim.x - 1) / blockDim.x
                ,(size_y + blockDim.y - 1) / blockDim.y);
    coalesced_coarse<size_x, size_y><<<gridDim, blockDim>>>(dst, 17);
    gpuErrchk ( cudaGetLastError() );
    gpuErrchk ( cudaDeviceSynchronize() );
  }

  gpuErrchk( cudaFree(dst));
  gpuErrchk( cudaDeviceReset() );
  return 0;
}

