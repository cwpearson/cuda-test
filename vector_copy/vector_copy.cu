#include <cstdio>

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

__global__ void kernel(long int *dst, const long int *src, int n) {
  int i = blockDim.x * blockIdx.x + threadIdx.x;
  if (i < n) {
    dst[i] = src[i];
  }
}

template<int SIZE_X, int SIZE_Y>
__global__ void coalesced(long int *dst, const long int *src) {
  int x = blockDim.x * blockIdx.x + threadIdx.x;
  int y = blockDim.y * blockIdx.y + threadIdx.y;
  if (x < SIZE_X && y < SIZE_Y) {
    dst[y * SIZE_X + x] = src[y * SIZE_X + x];
  }
}


template<int SIZE_X, int SIZE_Y>
__global__ void uncoalesced(long int *dst, const long int *src) {
  int y = blockDim.x * blockIdx.x + threadIdx.x;
  int x = blockDim.y * blockIdx.y + threadIdx.y;
  if (x < SIZE_X && y < SIZE_Y) {
    dst[y * SIZE_X + x] = src[y * SIZE_X + x];
  }
}

int main(int argc, char **argv) {

  long int *src, *dst;
  const int size_x = 1024;
  const int size_y = 1024;
  gpuErrchk ( cudaMalloc(&src, sizeof(long int) * size_x * size_y));
  gpuErrchk ( cudaMalloc(&dst, sizeof(long int) * size_x * size_y));

  const int block_x = 32;
  const int block_y = 32;
  dim3 blockDim(block_x, block_y);
  {
    dim3 gridDim(
      (size_x + block_x - 1) / block_x,
      (size_y + block_y - 1) / block_y
    );
    coalesced<size_x, size_y><<<gridDim, blockDim>>>(dst, src);
    gpuErrchk ( cudaGetLastError() );
    gpuErrchk ( cudaDeviceSynchronize() );
  }
  {
    dim3 gridDim(
      (size_y + block_x - 1) / block_x,
      (size_x + block_y - 1) / block_y
    );
    uncoalesced<size_x, size_y><<<gridDim, blockDim>>>(dst, src);
    gpuErrchk ( cudaGetLastError() );
    gpuErrchk ( cudaDeviceSynchronize() );
  }

  gpuErrchk( cudaFree(src));
  gpuErrchk( cudaFree(dst));

  gpuErrchk( cudaDeviceReset() );
  return 0;
}

