# Purpose

Simple code to get CUDA device properties and launch a kernel. Checks for errors.

# Testing NVCC

    make test_nvcc
    ./test_nvcc

# Testing Clang

    make test_clang
    ./test_clang
